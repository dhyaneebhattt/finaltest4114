//

//  RestaurantMapViewController.swift

//  InClassExercisesStarter

//

//  Created by parrot on 2018-11-22.

//  Copyright © 2018 room1. All rights reserved.

//



import UIKit

import Alamofire

import SwiftyJSON

import MapKit



class RestaurantMapViewController: UIViewController, MKMapViewDelegate {
    
    
    
    
    
    // MARK: Outlets
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    
    var lat = ""
    
    var lng = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        print("loaded the map screen")
        
        self.mapView.delegate = self
        
        
        
        mapApi()
        
    }
    
    
    
    // MARK: Actions
    
    
    
    @IBAction func zoomInPressed(_ sender: Any) {
        
        
        
        print("zoom in!")
        
        
        
        var map = mapView.region
        
        
        
        map.span.latitudeDelta = map.span.latitudeDelta / 4
        
        map.span.longitudeDelta = map.span.longitudeDelta / 4
        
        
        
        self.mapView.setRegion(map, animated: true)
        
    }
    
    
    
    @IBAction func zoomOutPressed(_ sender: Any) {
        
        // zoom out
        
        print("zoom out!")
        
        
        
        var map = mapView.region
        
        map.span.latitudeDelta = map.span.latitudeDelta * 2
        
        map.span.longitudeDelta = map.span.longitudeDelta * 2
        
        self.mapView.setRegion(map, animated: true)
        
        // HINT: Check MapExamples/ViewController.swift
        
    }
    
    
    
    
    
    func mapApi(){
        
        
        
        let url = "https://opentable.herokuapp.com/api/restaurants?city=Toronto&per_page=5"
        
        
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON {
            
            (response) in
            
            
            
            // -- put your code below this line
            
            
            
            let x = CLLocationCoordinate2DMake(43.6532, -79.3832)
            
            
            
            // pick a zoom level
            
            let y = MKCoordinateSpanMake(0.5, 0.5)
            
            
            
            // set the region property of the mapview
            
            let z = MKCoordinateRegionMake(x, y)
            
            self.mapView.setRegion(z, animated: true)
            
            
            
            if (response.result.isSuccess) {
                
                print("awesome, i got a response from the website!")
                
                print("Response from webiste: " )
                
                print(response.data)
                
                
                
                do {
                    
                    let json = try JSON(data:response.data!)
                    
                    print(json)
                    
                    // print("\(json["restaurants"][0]["name"])")
                    
                    let arr = [0, 1, 2, 3, 4]
                    
                    for i in arr
                        
                    {
                        
                        let pin = MKPointAnnotation()
                        
                        var lat = json["restaurants"][i]["lat"].double
                        
                        var lng = json["restaurants"][i]["lng"].double
                        
                        let x = CLLocationCoordinate2DMake(lat! , lng!)
                        
                        
                        
                        pin.coordinate = x
                        
                        
                        
                        // 3. OPTIONAL: add a information popup (a "bubble")
                        
                        pin.title = json["restaurants"][i]["name"].string
                        
                        
                        
                        // 4. Show the pin on the map
                        
                        self.mapView.addAnnotation(pin)
                        
                    }
                    
                }
                    
                catch {
                    
                    print ("Error while parsing JSON response")
                    
                }
                
                
                
            }
            
            
            
        }
        
    }
    
    /*
     
     // MARK: - Navigation
     
     
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
     // Get the new view controller using segue.destinationViewController.
     
     // Pass the selected object to the new view controller.
     
     }
     
     */
    
    
    
}



////
////  RestaurantMapViewController.swift
////  InClassExercisesStarter
////
////  Created by parrot on 2018-11-22.
////  Copyright © 2018 room1. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import SwiftyJSON
//import MapKit
//
//class RestaurantMapViewController: UIViewController {
//
//    // MARK: Outlets
//    @IBOutlet weak var mapView: MKMapView!
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        print("loaded the map screen")
//
//        
//        // ALAMOFIRE function: get the data from the website
//        Alamofire.request(URL, method: .get, parameters: nil).responseJSON
//            {
//                (response) in
//
//                // -- put your code below this line
//
//                if (response.result.isSuccess)
//                {
//                    print("awesome, i got a response from the website!")
//                    print("Response from webiste: " )
//                    print(response.data)
//
//                    do
//                    {
//                        let json = try JSON(data:response.data!)
//
//                        //print(json)
//
//                        // PARSING: grab the latitude and longitude
//                        print(json["latitude"])
//                        print(json["longitude"])
//
//
//                        //OPTIOAL: You could write the code like this:
//                        // let currently = json["currently"]
//                        // let temp = currently["temperature"]
//
//                        // or, you could write it like this (in one line)
//                        let temp = json["currently"]["temperature"]
//
//                        print("Temperature: \(temp)")
//
//                    }
//                    catch
//                    {
//                        print ("Error while parsing JSON response")
//                    }
//
//                }
//
//        }
//
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
//
//
//    // MARK: Actions
//
//    @IBAction func zoomInPressed(_ sender: Any) {
//
//        print("zoom in!")
//
//        // HINT: Check MapExamples/ViewController.swift
//    }
//
//    @IBAction func zoomOutPressed(_ sender: Any) {
//        // zoom out
//        print("zoom out!")
//
//        // HINT: Check MapExamples/ViewController.swift
//    }
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//}
